# In this simple example, an if statement is added to a for loop to check to see if a car with in a list
# is named 'bmw', if it is, it is converted to uppercase and then printed to the console
# otherwise (else) it is simply printed with the first character converted to uppercase


# Prints
# 'Audi'
# 'BMW'
# 'Subaru'
# 'Toyota'
cars = ['audi', 'bmw', 'subaru', 'toyota']

for car in cars:
    if car == 'bmw':
        print(car.upper())
    else:
        print(car.title())

# Conditional Statements
#  Much like in any programming language, the if statement checks for a boolean value, and returns a result
# Based off of whether the Boolean condition is True or False
#  This is tested using various operators such as the equals operator(==)

car = 'bmw'
# Double equals checks and returns a boolean value
# Returns True when inside of an if statement
car == 'bmw'

# After reassignment, returns False inside an if statement
car = 'audi'
car == 'bmw'

#Checking for inequality

# Similarly to other languages, there is also an inequality operator (!=)
requested_topping = 'mushrooms'

# Returns 'Hold the anchovies!'
if requested_topping != 'anchovies':
    print('Hold the anchovies!')

# Of course, you can also check for numerical equality
age = 18
# Returns True
age == 18

# And for inequality

answer = 17

if answer != 42:
    print('That is not the answer, please try again!')

# Other equality checks include the greater than and less than checks

age = 19
age < 21
# Returns true

age <= 21
# Returns true

age >= 21
# Returns false

age >= 21
# Returns false

# And statements, unlike other languages, instead of using the && operator to check for if two conditional statments are true, simply use the 'and' statement instead
age_0 = 22
age_1 = 23

if age_0 >= 21 and age_1 >= 21:
    print('True')
else:
    print("I don't know")

# And likewise the or operator is simply invoked using 'or' instaead of the double pipes found in other languages (eg ||)

age_0 = 22
age_1 = 18

if age_0 >= 21 or age_1 >= 21:
    print('Age 0 is greater than or equal to 21 OR Age 1 is greater than or equal to 21')
else:
    print('Age 0 is NOT greater than or equal to 21 OR Age 1 is NOT greater than or equal to 21')

# Checking whether a Value is in a List

requested_toppings = ['mushrooms', 'onions', 'pineapple']

if 'mushrooms' in requested_toppings:
    print('mushrooms is in the requested_toppings list!')
else:
    print('mushrooms is NOT in the requested_toppings list')

if 'pepperoni' in requested_toppings:
    print('pepperoni is in the requested_toppings list')
else:
    print('pepperoni is NOT in the requested_toppings list')

# Checking whether or not a Value is NOT in a list

banned_users = ['andrew', 'carolina', 'david']
user = 'marie'

if user not in banned_users:
    print(f"{user.title()}, you can post a response if you wish.")

# Boolean Expressions

game_active = True
can_edit = False

# if-elif-else chaining

# As an example let's consider the following conditions:

# Admission for anyone under age 4 is free.
# Admission for anyone between the ages of 4 and 18 is $25
# Admission for anyone age 18 or older is $40

age = 12

# Returns "Your admission cost is $25"
if age < 4:
    print("Your admission cost is $0")
elif age < 18:
    print("Your admission cost is $25")
else:
    print("Your admission cost is $40")

# You can also use the f() to change a variable(price) within the conditional statements based off the user age

age = 12

if age < 4:
    price = 0
elif age < 18:
    price = 25
else:
    price = 40

print(f"Your admission cost is ${price}")

# Just like in any other language you can chain elif blocks(rather self explanatory, see book, page 82 for details)
# You can also omit the else block just like most other languages, just include it when you want a default statement that covers
# all other conditions.

# Testing multiple conditions, you can check multiple conditions with multiple if statements when you need to test and have more than one test result

requested_toppings = ['mushrooms', 'extra cheese']

if 'mushrooms' in requested_toppings:
    print("Adding mushrooms.")
if 'pepperoni' in requested_toppings:
    print("Adding pepperoni.")
if 'extra cheese' in requested_toppings:
    print("Adding extra cheese.")

print("\nFinished making your pizza!")

# The above code returns
# "Adding mushrooms."
# "Adding extra cheese."
#
# "Finished making your pizza!"

# Note that this would not work with if-elif-else statements as it would stop looping over the requested_toppings list once one true statement had been found

# If statement within a for loop:

requested_toppings = ['mushrooms', 'green peppers', 'extra cheese']

for requested_topping in requested_toppings:
    if requested_topping == 'green peppers':
        print('Sorry, we are out of green peppers right now.')
    else:
        print(f'Adding {requested_topping}.')

print('\nFinished making your pizza!')

# Checking to see if list is not empty:

requested_toppings = []

if requested_toppings:
    for requested_topping in requested_toppings:
        print(f"Adding {requested_topping}")
    print('\nFinished making your pizza!')
else:
    print('Are you sure you want a plain pizza')

# Using and comparing 2 lists:

available_toppings = ['mushrooms', 'olives', 'green peppers', 'pepperoni', 'pineapple', 'extra cheese']

requested_toppings = ['mushrooms', 'french fries', 'extra cheese']

for requested_topping in requested_toppings:
    if requested_topping in available_toppings:
        print(f"Adding {requested_topping}")
    else:
        print(f"Sorry, we don't have {requested_topping}.")

print("\nFinished making your pizza!")


