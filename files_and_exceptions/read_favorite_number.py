import json

def read_number():
    """Read the favorite_number.json and print success message"""
    filename = 'favorite_number.json'
    with open(filename, 'r') as f:
        favorite_number = json.load(f)
    print(f"I know your favorite number! It's {favorite_number}")

read_number()
