# Much like JavaScript's try/catch blocks, Python has it's own version called tr/except, 
# which does very much the same thing.

# print(5/0)
# Returns a Traceback error and ends/crashes our program:
# Traceback (most recent call last):
  # File "/home/brian/Documents/Code/python/python_crash_course/files_and_exceptions/try_except.py", line 4, in <module>
    # print(5/0)
# ZeroDivisionError: division by zero

# With try/except syntax, we can not only present a more user-friendly message, we can also allow our
# program to end gracefully:

try:
    print(5/0)
except:
    print("You can't divide by zero!")
# Returns "You can't divide by zero!"
# Note that the following code will still run (until it reaches an unhandled error)

# A calculator that only does division:

# print("Give me two numbers, and I'll divide them.")
# print("Enter 'q' to quit.")

# while True:
    # first_number = input("\nFirst number: ")
    # if first_number == 'q':
        # break
    # second_number = input("Second number: ")
    # if second_number == 'q':
        # break
    # # The else Block
    # try:
        # answer = int(first_number) / int(second_number)
    # except ZeroDivisionError:
        # print("You can't divide by 0!")
    # else:
        # print(answer)

# Note that while the above while block works, if we enter any number followed by 0, it will once again give us the traceback error
# as before, this is fixed by utilizing the try/except/else block, which uses a try/except block first, and then if try works,
# we use else to print out the answer, the except clause prevents the traceback error from showing and also allows our program
# to continue working

# It is also good to note here that alot of information about our program is included in a Traceback call, and it is thusly
# encouraged that programmers not allow their programs to return the full error message as this can be used by malicious actors
# to "hack" our code.

# Handling the FileNotFoundError Exception

# Let's say we reference a file we don't have:
filename = 'alice.txt'

# Note the use of encoding= 'utf-8', which tells python what kind of encoding to expect when reading the file,
# we also use the variable name f to refer to within the with block, this is convention, and replaces the file_object syntax
# used earlier

# with open(filename, encoding= 'utf-8') as f:
    # contents = f.read()

# Returns:
# Traceback (most recent call last):
  # File "/home/brian/Documents/Code/python/python_crash_course/files_and_exceptions/try_except.py", line 54, in <module>
    # with open(filename, encoding= 'utf-8') as f:
# FileNotFoundError: [Errno 2] No such file or directory: 'alice.txt'

# now let's try using a try/except clause to handle the cases where alice.txt isn't found

try:
    with open(filename, encoding= 'utf-8') as f:
        contents = f.read()
except FileNotFoundError:
    print(f"Sorry, the file {filename} does not exist.")

# returns:
# Sorry, the file alice.txt does not exist.

# Analyzing Text

# Here we use Python's split() method, which will split up a string by spaces when no parameters are passed to it: 

title = 'Alice in Wonderland'
split_title = title.split()
print(split_title)
# returns:
# ['Alice', 'in', 'Wonderland']

# At this point in the exercise, let's copy and paste the entire book of Alice in Wonderland to a file called 'alice.txt',
# this can be found at this website: https://www.gutenberg.org/files/11/11-0.txt

# Now we can use split() and len() to count how many words are in the text document

# filename = 'alice.txt'

# try:
    # with open(filename, encoding= 'utf-8') as f:
        # contents = f.read()
# except FileNotFoundError:
    # print(f"Sorry, the file {filename} does not exist.")
# else:
    # # Count the approximate number of words in the file.
    # words = contents.split()
    # num_words = len(words)
    # print(f"The file {filename} has about {num_words} words.")

# returns:
# The file alice.txt has about 29594 words.

# Working with Multiple Files

# Here we're going to cover working with multiple files, alot of this will be repetitive, which usually means creating
# a function that can accept parameters through which we will pass the same logic, here we'll encapsulate our word counting program above in a function:

def count_words(filename):
    """Count the approximate number of words in the file."""
    try:
        with open(filename, encoding= 'utf-8') as f:
            contents = f.read()
    except FileNotFoundError:
        # Failing Silently
        # Sometimes, we don't need to let the user know that anything went wrong, we simply want the except statement to pass on:
        pass
        # Note that in this case, our "Sorry, the file siddhartha.txt does not exist." message will not print,
        # but our program will continue on without displaying any message to the user.
        # print(f"Sorry, the file {filename} does not exist.")
    else:
        words = contents.split()
        num_words = len(words)
        print(f"The file {filename} has about {num_words} words.")

# filename = 'alice.txt'
# count_words(filename)

# Now we can create a list of filenames to have words counted within them, using a for loop:

filenames = ['alice.txt', 'siddhartha.txt', 'moby_dick.txt', 'little_women.txt']
for filename in filenames:
    count_words(filename)

# returns:
# The file alice.txt has about 29594 words.
# Sorry, the file siddhartha.txt does not exist.
# The file moby_dick.txt has about 215711 words.
# The file little_women.txt has about 189142 words.

# Try It Yourself (page 201)
# def add_two_numbers():
    # """
    # Adds two numbers together
    # and throws custom error response if wrong type is passed.
    # """
    # number_one = input("Enter a number: ")
    # number_two = input("Enter another number: ")

    # try:
        # final_number = int(number_one) + int(number_two)
    # except ValueError:
        # print("Those aren't two numbers!")
    # else:
        # print(final_number)

# add_two_numbers()

# Try It Yourself (page 202)
print("Enter two numbers to be added together:")
print("Enter 'q' to quit.")

while True:
    number_one = input("Enter a number: ")
    if number_one == 'q':
        break
    number_two = input("Enter another number: ")
    if number_two == 'q':
        break
    else:
        try:
            final_number = int(number_one) + int(number_two)
        except ValueError:
            print("Those aren't two numbers!")
        else:
            print(final_number)


