filenames= ["cats.txt", "dogs.txt", "iguanas.txt"]

for filename in filenames:
    try:
        with open(filename, 'r') as f:
            list = f.readlines()
    except FileNotFoundError:
        pass
        # print(f"Sorry, {filename} was not found.")
    else:
        for line in list:
            print(line.rstrip())
