# Try It Yourself (page 202)

othello = "othello.txt"

with open(othello, 'r') as f:
    iago_count = 0
    lines = f.readlines()
    for line in lines:
        iago_count += line.count("Iago") + line.lower().count("Iago")
    print(f"The character, Iago, is mentioned {iago_count} times.")

