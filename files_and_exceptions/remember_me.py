import json

# Load the username, if it has been stored previously.
# Otherwise, prompt for the username and store it.

# def greet_user():
    # """Greet stored username if available."""
    # filename = 'username.json'
    # try:
        # with open(filename) as f:
            # username = json.load(f)
    # except FileNotFoundError:
        # username = input("What is your name? ")
        # with open(filename, 'w') as f:
            # json.dump(username, f)
            # print(f"We'll remember you when you come back, {username}!")
    # else:
        # print(f"Welcome back, {username}!")

# greet_user()

# Refactoring
# Here we divide up the logic from above a bit based off of what the specific function is doing, below
# We have get_stored_username() which does exactly as it is named, same goes for greet_user
def get_stored_username():
    """Get stored username if available."""
    filename = 'username.json'
    try:
        with open(filename) as f:
            username = json.load(f)
    except FileNotFoundError:
        return None
    else:
        return username

# and here we'll add one more function, get_new_username, which will prompt the user for a name, if the username doesn't exist
def get_new_username():
    """Prompt for a new username."""
    username = input("What is your name? ")
    filename = 'username.json'
    with open(filename, 'w') as f:
        json.dump(username, f)
    return username


def greet_user():
    """Greet the user by name."""
    username = get_stored_username()
    if username:
        verify_user = input(f"Is {username} the correct username?: ")
        if verify_user == 'yes':
            print(f"Welcome back, {username}!")
        else:
            get_new_username()
    else:
        # username = input("What is your name? ")
        username = get_new_username()
        filename = 'username.json'
        with open(filename, 'w') as f:
            json.dump(username, f)
            print(f"We'll remember you when you come back, {username}!")

greet_user()
