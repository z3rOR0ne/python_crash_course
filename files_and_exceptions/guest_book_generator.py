# Try it Yourself (page 193)

continue_prompt = True
while continue_prompt:
    name = input("What is your name?: ")
    if name == '':
        continue_prompt = False
    else:
        with open('guest_book.txt', 'a') as file_object:
            file_object.write(name + "\n")
