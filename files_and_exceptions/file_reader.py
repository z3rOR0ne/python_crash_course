# Much like NodeJS, Python is able to access the file system and read/write to it.

# Here we have an example of us opening the file 'pi_digits.txt', reading the contents of that file,
# and pritning it to the console.

# Much of this is straight forward, the open() method accepts a string that will look within the local
# directory for a file of that name, the as keyword renames that file as a variable to be used in this
# program.

# the read() method actually takes the data within the file and transcribes it(?)

# The with keyword closes the file once the code block following the colon(:) ends

# There is a close() method that closes the file once that line of the program is read,
# BUT it is not advisable to use it in this way because if an error occurs prior to that line,
# the program will crash and not end gracefully(?)

# The read() method also injects an empty "\n" line at the end of the contents, to remove this
# utilize the rstrip() method

with open('pi_digits.txt') as file_object:
    contents = file_object.read()
print(contents.rstrip())

# File Paths

# Much like in NodeJS (or bash or any other program with directories), Python looks for paths relative to the
# one from the program/.py file it is being called from, the following with statement would opne up a code
# block looking for a subdirectory within our current one called 'text_files' and then look for a file called
# 'filename.txt'

# with open('text_files/filename.txt') as file_object:

# Of course you could always use the absolute path as well, the above pi_digits.txt code block could start like this as well:

# file_path = '/home/brian/Documents/Code/python/python_crash_course/files_and_exceptions/pi_digits.txt')
# with open(file_path) as file_object: ...

# Reading Line by Line

# Here we assign the txt file to a variable, and using a for loop print the results line by line:
filename = 'pi_digits.txt'

with open(filename) as file_object:
    for line in file_object:
        print(line.rstrip())

# Making a List of Lines from a File

# The line's contents in the above example, however, are only accessible within the 'with' block, the following demonstrates
# a method called readlines() that within the block creates an array which can be returned.  This array has each line, as an
# element within the array:

filename = 'pi_digits.txt'

with open(filename) as file_object:
    lines = file_object.readlines()

for line in lines:
    print(line.rstrip())

# Working with a File's Contents

# Once we have access to the data within a file, we can manipulate however we'd like,
# let's create a string called pi_string that we'll create from our lines array

filename = 'pi_digits.txt'

with open(filename) as file_object:
    lines = file_object.readlines()

pi_string = ''
for line in lines:
    pi_string += line.rstrip()

print(lines)
# returns ['3.1415926535\n', '  8979323846\n', '  2643383279\n']
# Notice that the above returns \n at the end and some white space at the beginning of our lines array elements.


print(pi_string)
# Thusly this returns: 3.1415926535  8979323846  2643383279

print(len(pi_string))
# returns 36

# If we wish to concatenate our list back to a single line number, we can do so using the strip() method:

filename = 'pi_digits.txt'

with open(filename) as file_object:
    lines = file_object.readlines()

pi_string = ''
for line in lines:
    pi_string += line.strip()

print(pi_string)
# returns
# 3.141592653589793238462643383279
print(len(pi_string))
# returns
# 32

# Large Files: One Million Digits

filename = 'pi_million_digits.txt'

with open(filename) as file_object:
    lines = file_object.readlines()

pi_string = ''
for line in lines:
    pi_string += line.strip()

# print(pi_string)
# returns the entire string...
print(f"{pi_string[:52]}...")
# returns the first 52 characters of the string
print(len(pi_string))
# returns 1000002

# Is Your Birthday Contained in Pi?

# As a fun exercise, we can search to see if your birthday digits appear in the first million digits of pi

# birthday = input("Enter your birthday, in the form mmddyy: ")
# if birthday in pi_string:
    # print("Your birthday appears in the first million digits of pi!")
# else:
    # print("Your birthday does not appear in the first million digits of pi.")

# Replace method (Try it Yourself, page 191)

message = 'I really like dogs.'
message = message.replace('dog', 'cat')
print(message)

# Writing to a File

# Writing to an Empty File

filename = 'programming.txt'

# Here we see similar syntax as before, but we now are introduced to the second argument expected from open()
# We see a 'w', which indicates that we wish to 'write' a file, this creates a new file with the name expected in teh first argument
# The second parameter can receive a 'w'(write), a 'r'(read), or a 'a'(append)

# Note that 'w' will rewrite whatever is in the file if it exists, so if you wish to add to teh file, use hte 'a' parameter instead
with open(filename, 'w') as file_object:
    file_object.write('I love programming.\n')
    # Writing Multiple Lines
    file_object.write('I love creating new games.\n')

# Using bash's cat method, you can show the contents of this file in the terminal
# cat programming.txt
# returns:
# I love programming.
# I love creating games.

# Appending to a File

with open(filename, 'a') as file_object:
    file_object.write("I also love finding meaning in large datasets.\n")
    file_object.write("I love creating apps that can run in a browser.\n")

# cat programming.txt
# returns:
# I love programming.
# I love creating games.
# I also love finding meaning in large datasets.
# I love creating apps that can run in a browser.


