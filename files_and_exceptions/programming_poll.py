# Try it Yourself (page 193)

continue_prompt = True

while continue_prompt:
    user_input = input("Why do you like programming?: ")
    if user_input == '':
        continue_prompt = False
    else:
        with open('programmer_responses.txt', 'a') as file_object:
            file_object.write(user_input + "\n")
