# Try it Yourself (pg. 193)

filename = 'guest.txt'
user_input = input("What is your name?: ")

with open(filename, 'w') as file_object:
    file_object.write(user_input)
