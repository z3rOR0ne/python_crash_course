import json

numbers = [2, 3, 5, 7, 11, 13]

filename = 'numbers.json'

# Here we write the file to numbers.json
with open(filename, 'w') as f:
    # json.dump() takes two arguments
    # the first argument being a piece of data to store,
    # and the second being a file object it can store data in
    # in this case f, which is called 'as f' above
    json.dump(numbers, f)

# And here we read the file using the json.load() method
# this was intended to be inputted into another file as an example of a program pulling data from a file that
# was written by anothe program
with open(filename) as f:
    numbers = json.load(f)

# Prints
# [2, 3, 5, 7, 11, 13]
print(numbers)

