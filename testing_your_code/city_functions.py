your_city_name = input("Name a City: ")
your_country_name = input("And a Country: ")
your_population = input("And the population: ")

def location(city_name, country_name, population= ''):
    if population == "":
        return f"{city_name}, {country_name}".title()
    else:
        return f"{city_name}, {country_name}, ".title() + f"population={population}"

print(location(your_city_name, your_country_name, your_population))
