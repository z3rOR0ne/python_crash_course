# Testing a Class

# Testing a Class allows us to make adjustments to clases in a safe and effective manner,
# By creating unit tests for classes, we can ascertain whether or not our current class 
# is working, and if it is, we can safely assume that we can extend that class's properties
# or functionality without creating breaking changes.

# The following is copied from the Table 11-1 and documents the 
# Assert Methods available from python's unittest Module

# ---------------------------------------------------------------
# METHOD                                USE
# ---------------------------------------------------------------
# assertEqual(a, b)                     Verify that a == b
# assertNotEqual(a, b)                  Verify that a != b
# assertTrue(x)                         Verify that x is True
# assertFalse(x)                        Verify that x is False
# assertIn(item, list)                  Verify that item is in list
# assertNotIn(item, list)               Verify that item is not in list
# ---------------------------------------------------------------

# A Class to Test

class AnonymousSurvey:
    """Collect anonymous answers to a survey question."""

    def __init__(self, question):
        """Store a question, and prepare to store responses."""
        self.question = question
        self.responses = []

    def show_question(self):
        """Show the survey question."""
        print(self.question)

    def store_response(self, new_response):
        """Store a single response to the survey."""
        self.responses.append(new_response)

    def show_results(self):
        """Show all the responses that have been given."""
        print("Survey results:")
        for response in self.responses:
            print(f"- {response}")
