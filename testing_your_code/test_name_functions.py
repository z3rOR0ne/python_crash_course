# Unit Tests and Test Cases
# Here we are utilizing Python's native unittest module. Specifically, the unittest module tests whether the output of a function
# is that which is expected.'
# By performing unit tests, they are accumulated to create test cases. 

#A Passing Test

import unittest
from name_function import get_formatted_name

# unittest is expected within a class, where in this case we pass the unittest.TestCase subclass
# the self.assertEqual method then takes two arguments and compares the get_formatted_name()'s return value
# and compares it to the hard coded expected result, 'Janis Joplin'

class NamesTestCase(unittest.TestCase):
    """Tests for 'name_function.py'."""

    def test_first_last_name(self):
        """Do names like 'Janis Joplin' work?"""
        formatted_name = get_formatted_name('janis', 'joplin')
        self.assertEqual(formatted_name, 'Janis Joplin')

    def test_first_last_middle_name(self):
        """Do names like 'Wolfgang Amadeus Mozart' work?"""
        formatted_name = get_formatted_name(
            'wolfgang', 'mozart', 'amadeus')
        self.assertEqual(formatted_name, 'Wolfgang Amadeus Mozart')

# This is where unit testing may get confusing, as here we are checking to see if the local directory hard
# a kind of environment variable called __name__, and if it exists, is it equal to the __main__ environment variable,
# if so, then the unittest.main() method is called which actually invokes the test to be run when this file is called.
if __name__ == '__main__':
    unittest.main()
