# Try it Yourself 11-1 (pg. 215)

import unittest
from city_functions import location

class NamesTestCase(unittest.TestCase):
    """Tests for city_functions"""

    def test_city_name_country_name(self):
        """Do city/countries like 'Los Angeles, America' work?"""

        formatted_location = location('Los Angeles', 'America')
        self.assertEqual(formatted_location, 'Los Angeles, America')

    def test_city_country_population(self):
        """
        Do city/countries/population like 'santiago', 'chile', and 'population=500000' work?
        """

        formatted_location_population = location('santiago', 'chile', 500000)
        self.assertEqual(formatted_location_population, 'Santiago, Chile, population=500000')

if __name__ == '__main__':
    unittest.main()
