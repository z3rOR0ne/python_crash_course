from car import ElectricCar

# Right now, the child class is just a carbon copy of the parent class, so let's make an instance of our child class:
my_tesla = ElectricCar('tesla', 'model s', 2019)
print(my_tesla.get_descriptive_name())
# Describe Attributes and Methods for the Child Class 
my_tesla.describe_battery()
#Overriding Methods from the Parent Class
my_tesla.fill_gas_tank()
# Instances as Attributes
my_tesla.battery.describe_battery()
my_tesla.battery.get_range()


