from car_2 import Car
# Using Aliases
from electric_car import ElectricCar as EC

my_beetle = Car('volkswagen', 'beetle', 2019)
print(my_beetle.get_descriptive_name())

# Using Aliases
my_tesla = EC('tesla', 'roadster', 2019)
print(my_tesla.get_descriptive_name())
