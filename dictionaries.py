# Dictionaries are pretty much like JavaScript Objects, but in python

# A Simple Dictionary Example:
alien_0 = {'color': 'green', 'points': 5}

print(alien_0['color'])
print(alien_0['points'])

# You can then store the value of a particular key in a variable:
new_points = alien_0['points']
print(f"You just earned {new_points} points!")

# And also assign new key/value pairs like so:
alien_0['x_position'] = 0
alien_0['y_position'] = 25

print(alien_0)

# Creating a dictionary that is empty is sometimes necessary, and adding values to an empty dictionary is
# pretty much the same as adding new values like above:

alien_0 = {}

alien_0['color'] = 'green'
alien_0['points'] = 5

print(alien_0)

# Of course, because there are no constant variables in python, you can easily redefine the value of a key by
# referencing and then changing it like so:

print(f"The alien is {alien_0['color']}")
alien_0['color'] = 'yellow'
print(f"The alien is now {alien_0['color']}")

# An example using an if statement:

alien_0 = {'x_position': 0, 'y_position': 25, 'speed': 'fast'}
print(f"Original position: {alien_0['x_position']}")

# Move the alien to the right.
# Determine how far to move the alien based on its current speed.
if alien_0['speed'] == 'slow':
    x_increment = 1
elif alien_0['speed'] == 'medium':
    x_increment = 2
else:
    x_increment = 3

# The new position is the old position plus the increment.
alien_0['x_position'] = alien_0['x_position'] + x_increment

print(f"New position: {alien_0['x_position']}")

# Removing key/value pairs:

alien_0 = {'color': 'green', 'points': 5}
print(alien_0)

del alien_0['points']
print(alien_0)

# A dictionary of similar objects:

favorite_languages = {
    'jen': 'python',
    'sarah': 'c',
    'edward': 'ruby',
    'phil': 'python'
}

language = favorite_languages['sarah'].title()
print(f"Sarah's favorite language is {language}.")

# Using get() to Access Values:

# If a key doesn't exist within the dictionary, python will throw an error:
# Throws an error:

alien_0 = {'color': 'green', 'speed': 'slow'}
# print(alien_0['points'])

# The get() method takes two arguments, the first is the key we wish to find, and the second is a default value to be presented if no key is found:

point_value = alien_0.get('points', 'No point value assigned.')
print(point_value)

# Looping through key/value pairs:

#  A for loop can be used to loop over a dictionary, but it requirres the items() method to return references to the {key} and {value} like so:

user_0 = {
    'username': 'efermi',
    'first': 'enrico',
    'last': 'fermi',
}

for key, value in user_0.items():
    print(f"\nKey: {key}")
    print(f"Value: {value}")

# Of course we can loop through our favorite languages dictionary above in a similar manner:
favorite_languages = {
    'jen': 'python',
    'sarah': 'c',
    'edward': 'ruby',
    'phil': 'python'
}
for name, language in favorite_languages.items():
    print(f"{name.title()}'s favorite language is {language.title()}.")


# We can similarly use the keys() method to return only the keys from a loop over a dictionary:

for name in favorite_languages.keys():
    print(name.title())

# Note that .keys() is the default behavior when calling a simple for loop on a dictionary,
# so the following would return the same results:

for name in favorite_languages:
    print(name.title())

# Using the keys() method is optional, but is useful for readability.

# We can use an if statement in conjunction with a list to check for a value within a dictionary like so:
favorite_languages = {
    'jen': 'python',
    'sarah': 'c',
    'edward': 'ruby',
    'phil': 'python'
}

friends = ['phil', 'sarah']

# Looping Through a Dictionaries Keys in a Particular Order
# The following example demonstrates the use of the sorted() method that will sort a list
# (in this case in alphabetical order)

favorite_languages = {
    'jen': 'python',
    'sarah': 'c',
    'edward': 'ruby',
    'phil': 'python',
}

for name in sorted(favorite_languages.keys()):
    print(f"{name.title()}, thank you for taking the poll.")

# Looping Through All Values in a Dictionary
print("\nThe following languages have been mentioned:\n")
for language in favorite_languages.values():
    print(language.title())

# Very much like the JavaScript Set(), python also has a set() method that will only return
# unique values.

print("\nThe following UNIQUE languages have been mentioned:")
for language in set(favorite_languages.values()):
    print(language.title())

# A set is pretty much just a list with unique values, but if you want to create a set directly,
# you can use the curly braces to define it, just don't use colons, as that will make it a dictionary.
# Note that the repetition here is solely for demonstration purposes
languages = {'python', 'ruby', 'python', 'c'}
print("\n")
print(languages)
print("\n")
# Nesting

# Nesting is a familiar concept in programming languages, and it is no different in python,
# here let's create a list of dictionaries:

alien_0 = {'color': 'green', 'points': 5}
alien_1 = {'color': 'yellow', 'points': 10}
alien_2 = {'color': 'red', 'points': 15}

aliens = [alien_0, alien_1, alien_2]

for alien in aliens:
    print(alien)

# For in Range Loop
# The in range() syntax expects a number as an argument, which tells python how many times
# You'd like to loop over the following lines of code,
# Note that until now, we've used for in loops to simply loop over an entire list or dictionary,
# Our general familiarity in JavaScript has been a standard for loop which uses a .length property to determine
# the length of an array and loop over it, but in Python, the length property is implicit, range allows us to create
# a fixed number of times, very much how in JavaScript we could hard code a loop as such:
# for (var i = 0; i <= 10; i++) { etc...

# So let's take a look at in range:

# Make an empty list for storing aliens.

aliens = []

# Make 30 green aliens.
for alien_number in range(30):
    new_alien = {'color': 'green', 'points': 5, 'speed': 'slow'}
    aliens.append(new_alien)
print("\n")

# Show the first 5 aliens.
# Note, once again, the use of the [:] syntax
for alien in aliens[:5]:
    print(alien)
print("...")

# Show how many aliens have been created.
print(f"Total number of aliens: {len(aliens)}")
print("\n")

# Now let's do something a bit more complex, let's use an if statement within a for loop
# that iterates over the first 3 aliens, and changes their attributes(values) if their 'color' is 'green':

for alien in aliens[:3]:
    if alien['color'] == 'green':
        alien['color'] = 'yellow'
        alien['speed'] = 'medium'
        alien['points'] = 10

# Now let's print out our first five aliens, to demonstrate the changes made to the first 3:
for alien in aliens[:5]:
    print(alien)
print("...")

# We can also loop through the first 3 aliens again, and this time use an elif clause
# That changes the yellow aliens to red and also changes their other attributes(values) accordingly:

for alien in aliens[0:3]:
    if alien['color'] == 'green':
        alien['color'] = 'yellow'
        alien['speed'] = 'medium'
        alien['points'] = 10
    elif alien['color'] == 'yellow':
        alien['color'] = 'red'
        alien['speed'] = 'fast'
        alien['points'] = 15

# A List within a Dictionary

# Store information about a pizza being ordered.

pizza = {
    'crust': 'thick',
    'toppings': ['mushrooms', 'extra cheese']
}

# Summarize the ordered
print(f"You ordered a {pizza['crust']}-crust pizza "
      "with the following toppings:")

# A quick aside note here, \t here means a horizontal tab character
# Note how the pizze['toppings'] references the key reference 'toppings',
# which then loops over the array which is its value

for topping in pizza['toppings']:
    print(f"\t{topping}")

# Nesting for loops(a List within a Dictionary):

# Even basic nesting for loops can get a little complicated, so let's break it down a bit:
# Here is relatively straight forward, we have a dictionary with a series of names as keys,
# each key references a list of favorite languages.

favorite_languages = {
    'jen': ['python', 'ruby'],
    'sarah': ['c'],
    'edward': ['ruby', 'go'],
    'phil': ['python', 'haskell'],
}

# Now our first nested for loop in Python, 
# First we define our iterators as name, and languages
# then we reference the languages.items(), that way we can easily access the values within each name key

for name, languages in favorite_languages.items():
    # We simply print the name followed by a string
    print(f"\n{name.title()}'s favorite languages are:")
    # And then we create an iterator called language that loops over the languages list
    for language in languages:
        # And prints a tab character followed by the language(s)
        print(f"\t{language.title()}")

# It is worthy to note that excessive nesting is considered a code smell, and should generally be avoided

# A Dictionary within a Dictionary

# This is another example of nesting, which, again can get complicated quickly, so let's break it down, step by step:

# We have a dictionary, which contains two other dictionaries inside of it
users = {
    'aenstein': {
        'first': 'albert',
        'last': 'einstein',
        'location': 'princeton',
    },

    'mcurie': {
        'first': 'marie',
        'last': 'curie',
        'location': 'paris',
    },
}

# We establish a for loop, which establishes two different iterators, one called username, the other called user_info
# Once again the items() method to refer to the user dictionary's values
for username, user_info in users.items():
    
    # We reference the first key, which in this case is the first iterator username , this will return:
    # 'Username: aenstein'
    # 'Username: mcurie'
    print(f"\nUsername: {username}")
    
    # We then store the first and last name in a variable called full_name, note the use of the second iterator user_info
    full_name = f"{user_info['first']} {user_info['last']}"
    location = user_info['location']
    
    # We can then simply print those variables to the console, and we're done
    print(f"\tFull name: {full_name.title()}")
    print(f"\tLocation: {location.title()}")

# A note here, for the sake of simplicity, each dictionary's keys were all identical, this is inteded for KISS principles,
# Were each dictionary to have even slightly different (or, horrifically, completely different) keys, this would
# get complicated fast.


