# Importing individual modules
# from car import Car, ElectricCar

# my_new_car = Car('audi', 'a4', 2019)
# print(my_new_car.get_descriptive_name())

# my_tesla = ElectricCar('tesla', 'roadster', 2019)
# print(my_tesla.get_descriptive_name())

# my_new_car.odometer_reading = 23
# my_new_car.read_odometer()
# my_new_car.fill_gas_tank()

#Importing an entire module
import car
# Alternatively you can also use
# from car import *
# But you don't have to use the 'car.' syntax, this method is not recommended

my_beetle = car.Car('volkswagen', 'beetle', 2019)
print(my_beetle.get_descriptive_name())

my_tesla = car.ElectricCar('tesla', 'roadster', 2019)
print(my_tesla.get_descriptive_name())
