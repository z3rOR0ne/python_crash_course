# Alternatively we could also be more specific with our import like so:

# from pizza import make_pizza

# And then we could just call it like so:

# make_pizza(16, 'pepperoni')

import pizza

pizza.make_pizza(16, 'pepperoni')
pizza.make_pizza(12, 'mushrooms', 'green peppers', 'extra cheese')

# You can also be more specific and import a specific function (or list/dictionary/tuple I suspect) by using the following syntax:

# from module_name import function_name

# Or multiple functions like so:

# from module import function_0, function_1 , function_2

# Using as to Give a Function an Alias

# from pizza import make_pizza as mp

# mp(16, 'pepperoni')
# mp(12, 'mushrooms', 'green peppers', 'extra cheese')

# Using as to Give a Module an Alias

# import pizza as p

# p.make_pizza(16, 'pepperoni')
# p.make_pizza(12, 'mushrooms', 'green peppers', 'extra cheese')

# Or if you just wanted to get greedy and import everything you could:
# Import All Functions in a Module

from pizza import *

make_pizza(16, 'pepperoni')
make_pizza(12, 'mushrooms', 'green peppers', 'extra cheese')


