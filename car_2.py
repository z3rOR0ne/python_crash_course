"""A class that can be used to represent a car."""

class Car:
    """A simple attempt to represent a car."""

    def __init__(self, make, model, year):
        """Initialize attributes to describe a car."""
        self.make = make
        self.model = model
        self.year = year
        # Setting a default value is easy, we simply don't define the attribute in our arguments above, and assign a static value here:
        self.odometer_reading = 0


    # Here we define a method that simply prints our class's information neatly, this
    # allows us to not have to write the print() statement multiple times(as long as we don't wish to change the format)
    def get_descriptive_name(self):
        """Return a neatly formatted descriptive name."""
        long_name = f"{self.year} {self.make}  {self.model}"
        return long_name.title()

    def read_odometer(self):
        """Print a statement showing the car's mileage"""
        print(f"This car has {self.odometer_reading} miles on it.")

    # Modifying an Attribute's Value Through a Method

    def update_odometer(self, mileage):
        """
        Set the odometer reading to the given value.
        Reject the change if it attempts to roll the odometer back.
        """
        # as long as the mileage is greater than or equal to the odometer reading
        if mileage >= self.odometer_reading:
            # reset the odometer reading to the mileage passed
            self.odometer_reading = mileage
        else:
            # otherwise the user has tried to pass a number smaller than the set odometer mileage
            # aka rolling back the odomter, which is illegal
            # and therefore we should not allow the user to do so.
            print("You can't roll back an odometer!")

    def increment_odometer(self, miles):
        """Add the given amount to the odometer reading."""
        self.odometer_reading += miles

    def fill_gas_tank(self):
        """Print a statement showing that the gas tank is being filled."""
        print(f"Filling up car with gas...")
