class Dog:
    """A simple attempt to model a dog"""
    # Note the special syntax here with __init__, which is a special method that initializes our class.
    # __init__ can be though tof as a special method that takes arguments that instantiate an instance of something
    # Similar in nature to a dictionary (self, is similar to the this keyword in JavaScript)
    def __init__(self, name, age):
        """Initialize name and age attributes."""
        self.name = name
        self.age = age

    def sit(self):
        """Simulate a dog sitting in response to a command."""
        print(f"{self.name} is now sitting.")

    def roll_over(self):
        """Simulate rolling over in response to a command."""
        print(f"{self.name} rolled over!")

my_dog = Dog('Willie', 6)

print(f"My dog's name is {my_dog.name}")
print(f"My dog is {my_dog.age} years old.")

print(f"Sit {my_dog.name}!")
my_dog.sit()
print(f"Good boy {my_dog.name}! Now roll over.")
my_dog.roll_over()

your_dog = Dog('Lucy', 3)

print(f"\nYour dog's name is {your_dog.name}.")
print(f"Your dog is {your_dog.age} years old.")
your_dog.sit()
your_dog.roll_over()

class Restaurant:
    """A simple model to make a restaurant"""

    def __init__(self, restaurant_name, cuisine_type):
        """Initialize restaurant_name and cuisine_type attributes."""
        self.restaurant_name = restaurant_name
        self.cuisine_type = cuisine_type
        #Exercises
        self.number_served = 0

    def describe_restaurant(self):
        """Prints a Brief Description of the Restaurant"""
        print(f"My restaurant's name is {self.restaurant_name} and we serve {self.cuisine_type}.")

    def open_restaurant(self):
        """Simulate opening up the restaurant"""
        print(f"{self.restaurant_name} is now open for business!")

    # Exercises
    def set_number_served(self, more_served):
        """Updates the Number Served"""
        self.number_served += more_served


my_restaurant = Restaurant('Huaraches', 'Mexican Food')
my_restaurant.describe_restaurant()
print(f"{my_restaurant.restaurant_name} has served {my_restaurant.number_served} customers so far.")
my_restaurant.open_restaurant()
my_restaurant.number_served = 10
print(f"{my_restaurant.restaurant_name} has served {my_restaurant.number_served} customers so far.")
my_restaurant.set_number_served(10)
print(f"{my_restaurant.restaurant_name} has served {my_restaurant.number_served} customers so far.")

your_restaurant = Restaurant("Tony's", 'American Steaks')
george_restaurant = Restaurant("Lilly's", 'Brain Food')

your_restaurant.describe_restaurant()
george_restaurant.describe_restaurant()

class User:

    def __init__(self, first_name, last_name, age, marriage_status, job):
        """A classic user model"""
        self.first_name = first_name
        self.last_name = last_name
        self.age = age
        self.marriage_status = marriage_status
        self.job = job
        # Exercises
        self.login_attempts = 0

    def describe_user(self):
        """Prints out a formatted profile of the User"""
        print("\n--- User Profile ---")
        print(f"\n\tName: {self.first_name} {self.last_name}\n\tAge: {self.age}\n\tMarital Status: {self.marriage_status}\n\tJob: {self.job}")
        print("\n--- End of Profile ---")

    def greet_user(self):
        """Displays a Custom Greeting to the User"""
        print(f"Hello there {self.first_name} {self.last_name}! It's nice to see you agaain.\nI see that you are {self.age} years old,\nyou are {self.marriage_status},\nand you work as a {self.job}. I look forward to getting to know you further!")

    def increment_login_attempts(self):
        """Increments the Login Attempts by 1"""
        self.login_attempts = self.login_attempts + 1

    def reset_login_attempts(self):
        """Resets the Login Attempts to 0"""
        self.login_attempts = 0


jenny = User('Jenny', 'Neilan', 35, 'married', 'libriarian')
print(f"Jenny has attempted to login {jenny.login_attempts} times.")
jenny.increment_login_attempts()
jenny.increment_login_attempts()
jenny.increment_login_attempts()
print(f"Jenny has attempted to login {jenny.login_attempts} times.")
jenny.reset_login_attempts()
print(f"Jenny has attempted to login {jenny.login_attempts} times.")
bob = User('Bob', 'Filthy', 59, 'single', 'creep')
billy = User('Billy', 'Barbtoy', 12, 'single', 'fireman')

print("\n")
jenny.greet_user()
print("\n")
bob.greet_user()
print("\n")
billy.greet_user()

print("\n")
# Working with Classes and Instances

class Car:
    """A simple attempt to represent a car."""

    def __init__(self, make, model, year):
        """Initialize attributes to describe a car."""
        self.make = make
        self.model = model
        self.year = year
        # Setting a default value is easy, we simply don't define the attribute in our arguments above, and assign a static value here:
        self.odometer_reading = 0


    # Here we define a method that simply prints our class's information neatly, this
    # allows us to not have to write the print() statement multiple times(as long as we don't wish to change the format)
    def get_descriptive_name(self):
        """Return a neatly formatted descriptive name."""
        long_name = f"{self.year} {self.make}  {self.model}"
        return long_name.title()

    def read_odometer(self):
        """Print a statement showing the car's mileage"""
        print(f"This car has {self.odometer_reading} miles on it.")

    # Modifying an Attribute's Value Through a Method

    def update_odometer(self, mileage):
        """
        Set the odometer reading to the given value.
        Reject the change if it attempts to roll the odometer back.
        """
        # as long as the mileage is greater than or equal to the odometer reading
        if mileage >= self.odometer_reading:
            # reset the odometer reading to the mileage passed
            self.odometer_reading = mileage
        else:
            # otherwise the user has tried to pass a number smaller than the set odometer mileage
            # aka rolling back the odomter, which is illegal
            # and therefore we should not allow the user to do so.
            print("You can't roll back an odometer!")

    def increment_odometer(self, miles):
        """Add the given amount to the odometer reading."""
        self.odometer_reading += miles

    def fill_gas_tank(self):
        """Print a statement showing that the gas tank is being filled."""
        print(f"Filling up car with gas...")

my_new_car = Car('audi', 'a4', 2019)
print(my_new_car.get_descriptive_name())
my_new_car.read_odometer()
my_new_car.fill_gas_tank()

# Modifying Attribute Values

# Modifying Attribute Values Directly

my_new_car.odometer_reading = 23
# Now returns "This car has 23 miles on it."
my_new_car.read_odometer()

# Modifying an Attribute's Value Through a Method
my_new_car.update_odometer(56)
my_new_car.read_odometer()

# Preventing rolling back the odometer with an if clause:
my_new_car.update_odometer(2)
my_new_car.read_odometer()

print("\n")
# Incrementing an Attribute's Value Through a Method

my_used_car = Car('subaru', 'outback', 2015)
print(my_used_car.get_descriptive_name())

my_used_car.update_odometer(23_500)
my_used_car.read_odometer()

my_used_car.increment_odometer(100)
my_used_car.read_odometer()

print("\n")
# Inheritance

# Class inheritance is basically when one class inherits attributes and methods from another Class and then extends those attributes to
# create a new, slightly (sometimes not so slightly) different Class.  The original Class is referred to as the parent class,
# while the New Class is referred to as the child class.


# Instances as Attributes
# As classes grow in size due to adding more and more attributes and methods, sometimes it's a good idea to create
# a separate class that holds attributes or methods that can be used by other classes, you can then instantiate them by calling on other classes
class Battery():
    """A simple attempt to model a battery for an electric car."""

    def __init__(self, battery_size=75):
        """Initialize the battery's attributes."""
        self.battery_size = battery_size

    def describe_battery(self):
        """Print a statement describing the battery size."""
        print(f"This car has a {self.battery_size}-kWh battery.")

    def get_range(self):
        """Print a statement about the range this battery provides."""
        if self.battery_size == 75:
            range = 260
        elif self.battery_size == 100:
            range = 315

        print(f"This car can go about {range} miles on a full charge.")

    # Exercise 9-9
    def upgrade_battery(self):
        if self.battery_size != 100:
            self.battery_size = 100
        else:
            self.battery_size = 100


class ElectricCar(Car):
    """Represents aspects of a car, specific to electric vehicles."""

    def __init__(self, make, model, year):
        """
        Initialize attributes of the parent class.
        Then initialize attributes specific to an electric car.
        """

        # Note the use of the keyword super() here.
        # This method allows us to call a method from the Parent Class (or 'super' class'),
        # In this case we call the __init__() method

        super().__init__(make, model, year)

        # Describe Attributes and Methods for the Child Class
        # Here we careate an attribute that is unique to ElectricCar, it does NOT inherit this from the Car parent class.
        self.battery_size = 75

        # Instances as Attributes
        self.battery = Battery()

    # And here we define a method unique to ElectricCar, which prints this unique attribute
    def describe_battery(self):
        """Print a statement describing the battery size."""
        print(f"This car has a {self.battery_size}-kWh battery.")

    # Overriding Methods from the Parent Class
    def fill_gas_tank(self):
        """Overrides Car.fill_gas_tank() method with different message"""
        print("This car doesn't need a gas tank!")

# Right now, the child class is just a carbon copy of the parent class, so let's make an instance of our child class:
my_tesla = ElectricCar('tesla', 'model s', 2019)
print(my_tesla.get_descriptive_name())
# Describe Attributes and Methods for the Child Class
my_tesla.describe_battery()
#Overriding Methods from the Parent Class
my_tesla.fill_gas_tank()
# Instances as Attributes
my_tesla.battery.describe_battery()
my_tesla.battery.get_range()

print("\n")

# Exercises 9-6
class IceCreamStand(Restaurant):
    """Defining a child class that inherits from the Restaurant Class"""

    def __init__(self, restaurant_name, cuisine_type):
        super().__init__(restaurant_name, cuisine_type)
        self.flavors = ['vanilla', 'chocolate', 'strawberry', 'pistachio', 'rocky road']

    def list_flavors(self):
        for flavor in self.flavors:
            print(flavor)

my_ice_cream_stand = IceCreamStand("Brian's Ice Cream", "dessert")
my_ice_cream_stand.list_flavors()

# Exercises 9-7
class Privileges:
    """Creates a Class to extend out into other classes, expects an array of privileges"""
    def __init__(self, privileges):
        self.privileges = []

# Exercises 9-6
class Admin(User):
    """Defining a child class that inherits from the User Class"""

    def __init__(self, first_name, last_name, age, marriage_status, job):
        super().__init__(first_name, last_name, age, marriage_status, job)
        self.privileges = Privileges(['can add post', 'can delete post', 'can ban user'])

    def show_privileges(self):
        for privelege in self.privileges.privileges:
            print(privelege)

my_admin = Admin('Bradly', 'Haynes', 47, 'single', 'systems administrator')
my_admin.show_privileges()

# Exercise 9-9

your_electric_car = ElectricCar('nikola', 'model t', 2021)
your_electric_car.battery.get_range()
your_electric_car.battery.upgrade_battery()
your_electric_car.battery.get_range()



