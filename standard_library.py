from random import randint
random_number = randint(1, 6)
# Returns random number between 1 and 6
print(random_number)

from random import choice
players = ['charles', 'martina', 'michael', 'florence', 'eli']
first_up = choice(players)
# Returns random choice form players list
print(first_up)

# Exercise 9-13

class Die():

    def __init__(self, sides):
            self.sides = 6 

    def roll_die(self):
        print(randint(1, self.sides))

my_die = Die(6)
print(f"\nRolling my {my_die.sides} sided die 6 times")
for i in range(6):
    my_die.roll_die()

my_die = Die(10)
my_die.sides = 10
print(f"\nRolling my {my_die.sides} sided die 10 times")
for i in range(10):
    my_die.roll_die()

my_die = Die(20)
my_die.sides = 20
print(f"\nRolling my {my_die.sides} sided die 10 times")
for i in range(10):
    my_die.roll_die()

# Exercise 9-14

numbers_letters = [
    'a',
    'b',
    'c',
    'd',
    'e',
    0,
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
]
def loop_through():
    lottery_numbers = []
    for i in range(4):
        random_choice = choice(numbers_letters)
        if type(random_choice) is str:
            lottery_numbers.append(random_choice)
        elif type(random_choice) is int:
            random_choice = str(random_choice)
            lottery_numbers.append(random_choice)
    lottery_numbers = ''.join(lottery_numbers)
    return f"The following lottery numbers win a prize!: {lottery_numbers}"

print("\n")
print(loop_through())

# Exerfcise 9-15

print("\n")
# Pretty much impossible to win, the program recursively runs utnil my_ticket is found, but the chances are too difficult, python poops out
# random_lotto = loop_through()
# my_ticket = ['e3d4', '819e', 'a48d']
# def pick_winner():
    # tries = 0
    # if my_ticket == random_lotto:
        # return f"You won in {tries} many tries!"
    # elif tries <= 20:
        # tries += 1
        # pick_winner()
    # elif tries > 20:
        # return f"You didn't win and you tried {tries} times!"

# print(pick_winner())



