# Python provides a built in function for user input(), similar to nodejs's prompt or npm's promptsync modules

# message = input("Tell me something, and I will repeat it back to you: ")
# print(message)

# Very much like JavaScript string literal syntax, this can be used for custom messages based off of user input:

# name = input("Please enter your name: ")
# print(f"\nHello, {name}!")

# You can also pass hard coded variables to the input function

# prompt = "IF you tell us who you are, we can personalize the messages you see."
# prompt += "\nWhat is your first name? "

# name = input(prompt)
# print(f"\nHello, {name}")

# input() is limited in that it will always return a string, but what if we wanted to return user input as a number?
# We'll need to use a type conversion method, in this case, we can use the int() method:

# age = input("How old are you? ")

# age = int(age)

# print(age >= 18)

# Another example we could use with int is to check to see if someone is tall enough to ride a particular ride:

# height = input("How tall are you, in inches? ")
# height = int(height)

# if height >= 48:
    # print("\nYou're tall enough to ride!")
# else:
    # print("\nYou'll be able to ride when you're a little older.")

# A quick note on the modulo operator
# Very much like in other programming languages, Python makes use of the modulo or % operator,
# Which returns the remainder of two number's divided by each other like so:

# print(5 % 3)
# Returns 2

# print(6 % 3)
# Returns 0

# print(7 % 3)
# Returns 1

# So very much like it's classic use in the Fizz/Buzz problem, we can use division to determine if a number is even or odd:

# number = input("Enter a number, and I'll tell you if it's even or odd: ")
# number = int(number)

# if number % 2 == 0:
    # print(f"\nThe number {number} is even.")
# else:
    # print(f"\nThe number {number} is odd.")

# Introducing while Loops

# While loops are a staple in other programming languages, and Python is no different, let's take a look:

# current_number = 1
# while current_number <= 5:
    # print(current_number)
    # current_number += 1

# So let's do something interesting, let's allow the user to determine when to quit a simple prompt program, like so:

# prompt= input("\nTell me something, and I will repeat it back to you: ")
# prompt += "\nEnter 'quit' to end the program. "

# message = ""
# while message != 'quit':
    # message = input(prompt)
    # if message != 'quit':
        # print(message)

# Using a Flag

# A flag can be thought of a variable that holds a value which determine whether or program is to continue to run.


# prompt= input("\nTell me something, and I will repeat it back to you: ")
# prompt += "\nEnter 'quit' to end the program. "

# active = True
# while active:
    # message = input(prompt)
    # if message == 'quit':
        # active = False
    # else:
        # print(message)

# In the above example, we set active to True.  This acts as our flag, and as long as it is true, will continue to return
# prompted message back to the user.  only if the user types 'quit', will the flag, active, become False, and the program ends

# Using a break statement to Exit a Loop:
# Another way to get the same effect is to use a 'break' statement in an if statement,
# this can be best utilized within a while clause where True is passed, keep in mind that this
# condition without a break statement will run forever, so it is best to put a break statement at somepoint when using
# while True:

# prompt = "\nPlease enter the name of a city you have visited: "
# prompt += "\n(Enter 'quit' when you are finished.)"

# while True:
    # city = input(prompt)

    # if city == 'quit':
        # break
    # else:
        # print(f"I'd love to go to {city.title()}!")

# Using continue in a Loop
# The following simply prints any odd number to the console, and ignores any even numbers
# it first increments the iterator current_number by 1,
# then, it loops over ten times as defined within the while clause, and then uses an if check with modulo to check for even numbers
# The continue statement tells Python to return to the while clause if the if statement returns true (iterator number is even)
# otherwise it prints the odd number to the console

# current_number = 0

# while current_number < 10:
    # current_number += 1
    # if current_number % 2 == 0:
        # continue

    # print(current_number)

# Avoiding infinite loops
# Obviously, running an infinite loop is a pointless waste of computer resources and should be avoided, here is a simple example
# of how an infinite loop could be instantiated, but as a program gets more complicated, it can become harder to see when we have
# accidentally created an infinite loop:

# x = 1
# Prints 1 forever
# while x <= 5:
    # print(x)

# Using a while loop with Lists and Dictionaries: 

# Moving Items from One List to Another
# Start with users that need to be verified,
# and an empty list to hold confirmed users.

# Verify each user until there are no more unconfirmed users.
# Move each verified user into the list of confirmed users.

# Here we have a while loop followed by a for loop. It is a tiny bit complicated so let's break it down a bit:

# we start off with a list of unconfirmed_ user and an empty list of confrimed_users
unconfirmed_users = ['alice', 'brian', 'candace']
confirmed_users = []


# while the unconfirmed_users list still has something in it...
# while unconfirmed_users:
    # we create a variable called current_user that is equal to the last element in the uncon list...
    # current_user = unconfirmed_users.pop()

    # we let the user know that the program is "Verifying" the user
    # print(f"Verifying user: {current_user.title()}"'')
    # when in reality, we're just popping it off the end of one list and appending it to the end of another
    # confirmed_users.append(current_user)

# Display all confirmed users.
# print("\nThe following users have been confirmed:")
# now we simply print whatever is within the confirmed_users list back to the user using a for loop
# for confirmed_user in confirmed_users:
    # print(confirmed_user.title())

# Removing All Instances of Sp;ecific Values from a List

# Demonstrating with if statements in the previous segment, we were able to use the remove() method to
# remove a single instance of a specific value from a list, but what if we wanted to remove ALL instances
# of a specific value from a list, it's easy! using a while loop solves our problem:

# pets= ['dog', 'cat', 'dog', 'goldfish', 'cat', 'rabbit', 'cat']
# print(pets)

# while 'cat' in pets:
    # pets.remove('cat')

# print(pets)

# Filling a Dictionary with User Input

# Now we can get cooking with something that starts to look like a todo list, here we take user input and
# put the values in a dictionary.  We're going to use a flag within a loop, here we go:

responses = {}

# Set a flag to indicate that polling is active.
polling_active = True

while polling_active:
    # Prompt for the person's name and response.
    name = input("\nWhat is your name? ")
    response = input("Which mountain would you like to climb somday? ")

    # Store the response in the dictionary.
    responses[name] = response

    # Find out if anyone else is going to take the poll.
    repeat = input("Would you like to let another person respond? (yes/no) ")
    if repeat == 'no':
        polling_active = False

# Polling is complete. Show the results.
print("\n--- Poll Results ---")
for name, response in responses.items():
    print(f"{name} would like to climb {response}.")
