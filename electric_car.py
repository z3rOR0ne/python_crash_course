from car_2 import Car

class Battery():
    """A simple attempt to model a battery for an electric car."""

    def __init__(self, battery_size=75):
        """Initialize the battery's attributes."""
        self.battery_size = battery_size

    def describe_battery(self):
        """Print a statement describing the battery size."""
        print(f"This car has a {self.battery_size}-kWh battery.")

    def get_range(self):
        """Print a statement about the range this battery provides."""
        if self.battery_size == 75:
            range = 260
        elif self.battery_size == 100:
            range = 315

        print(f"This car can go about {range} miles on a full charge.")

    # Exercise 9-9
    def upgrade_battery(self):
        if self.battery_size != 100:
            self.battery_size = 100
        else:
            self.battery_size = 100

class ElectricCar(Car):
    """Represents aspects of a car, specific to electric vehicles."""

    def __init__(self, make, model, year):
        """
        Initialize attributes of the parent class.
        Then initialize attributes specific to an electric car.
        """

        # Note the use of the keyword super() here.
        # This method allows us to call a method from the Parent Class (or 'super' class'),
        # In this case we call the __init__() method

        super().__init__(make, model, year)

        # Describe Attributes and Methods for the Child Class 
        # Here we careate an attribute that is unique to ElectricCar, it does NOT inherit this from the Car parent class.
        self.battery_size = 75

        # Instances as Attributes
        self.battery = Battery()

    # And here we define a method unique to ElectricCar, which prints this unique attribute
    def describe_battery(self):
        """Print a statement describing the battery size."""
        print(f"This car has a {self.battery_size}-kWh battery.")

    # Overriding Methods from the Parent Class
    def fill_gas_tank(self):
        """Overrides Car.fill_gas_tank() method with different message"""
        print("This car doesn't need a gas tank!")

