"""A class that can be used to represent a car."""

class Car:
    """A simple attempt to represent a car."""

    def __init__(self, make, model, year):
        """Initialize attributes to describe a car."""
        self.make = make
        self.model = model
        self.year = year
        # Setting a default value is easy, we simply don't define the attribute in our arguments above, and assign a static value here:
        self.odometer_reading = 0


    # Here we define a method that simply prints our class's information neatly, this
    # allows us to not have to write the print() statement multiple times(as long as we don't wish to change the format)
    def get_descriptive_name(self):
        """Return a neatly formatted descriptive name."""
        long_name = f"{self.year} {self.make}  {self.model}"
        return long_name.title()

    def read_odometer(self):
        """Print a statement showing the car's mileage"""
        print(f"This car has {self.odometer_reading} miles on it.")

    # Modifying an Attribute's Value Through a Method

    def update_odometer(self, mileage):
        """
        Set the odometer reading to the given value.
        Reject the change if it attempts to roll the odometer back.
        """
        # as long as the mileage is greater than or equal to the odometer reading
        if mileage >= self.odometer_reading:
            # reset the odometer reading to the mileage passed
            self.odometer_reading = mileage
        else:
            # otherwise the user has tried to pass a number smaller than the set odometer mileage
            # aka rolling back the odomter, which is illegal
            # and therefore we should not allow the user to do so.
            print("You can't roll back an odometer!")

    def increment_odometer(self, miles):
        """Add the given amount to the odometer reading."""
        self.odometer_reading += miles

    def fill_gas_tank(self):
        """Print a statement showing that the gas tank is being filled."""
        print(f"Filling up car with gas...")

class Battery():
    """A simple attempt to model a battery for an electric car."""

    def __init__(self, battery_size=75):
        """Initialize the battery's attributes."""
        self.battery_size = battery_size

    def describe_battery(self):
        """Print a statement describing the battery size."""
        print(f"This car has a {self.battery_size}-kWh battery.")

    def get_range(self):
        """Print a statement about the range this battery provides."""
        if self.battery_size == 75:
            range = 260
        elif self.battery_size == 100:
            range = 315

        print(f"This car can go about {range} miles on a full charge.")

    # Exercise 9-9
    def upgrade_battery(self):
        if self.battery_size != 100:
            self.battery_size = 100
        else:
            self.battery_size = 100

class ElectricCar(Car):
    """Represents aspects of a car, specific to electric vehicles."""

    def __init__(self, make, model, year):
        """
        Initialize attributes of the parent class.
        Then initialize attributes specific to an electric car.
        """

        # Note the use of the keyword super() here.
        # This method allows us to call a method from the Parent Class (or 'super' class'),
        # In this case we call the __init__() method

        super().__init__(make, model, year)

        # Describe Attributes and Methods for the Child Class
        # Here we careate an attribute that is unique to ElectricCar, it does NOT inherit this from the Car parent class.
        self.battery_size = 75

        # Instances as Attributes
        self.battery = Battery()

    # And here we define a method unique to ElectricCar, which prints this unique attribute
    def describe_battery(self):
        """Print a statement describing the battery size."""
        print(f"This car has a {self.battery_size}-kWh battery.")

    # Overriding Methods from the Parent Class
    def fill_gas_tank(self):
        """Overrides Car.fill_gas_tank() method with different message"""
        print("This car doesn't need a gas tank!")

