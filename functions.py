# Defining a Function

# The most basic of functions can be defined using the def keyword followed by a classic example of function syntax:

# def greet_user():
    # """Display a simple greeting."""
    # print("Hello!")

# greet_user()

# Like all other C based programming languages, functions in Python can take any number of arguments within the parentheses, like so:

def greet_user(username):
    """Display a simple greeting."""
    print(f"Hello, {username.title()}!")

greet_user('jesse')

# Positional Arguments

def describe_pet(animal_type, pet_name):
    """Display information about a pet."""
    print(f"\nI have a {animal_type}.")
    print(f"My {animal_type}'s name is {pet_name.title()}.")

describe_pet('hamster', 'harry')

# And we can pass it as many times as we like:

describe_pet('dog', 'fido')
describe_pet('cat', 'fifi')

# We have to order our parameters correctly in order to get the expected results,
# HOWEVER, we can call our parameters out of order if we explicitly tell Python what the values
# of the expected parameters are within the function call's arguments, like so:

describe_pet(pet_name= 'mochi', animal_type= 'cat')

# Default Values
# We can use similar syntax to create default values for our arguments within your function definition:

def describe_pet(pet_name, animal_type= 'dog'):
    """Display information about a pet."""
    print(f"\nI have a {animal_type}.")
    print(f"My {animal_type}'s name is {pet_name.title()}.")

# This way, we don't have to NECESSARILY define animal_type for our function describe_pet() to work:
describe_pet(pet_name='willie')

# Returning Values

# Returning a Simple Values

# The following demonstrates the simple use of a return statement, which acts very much like how it does in other
# programming languages, where a value is returned from the function, and the function ends.

def get_formatted_name(first_name, last_name):
    """Return a full name, neatly formatted."""
    full_name = f"{first_name} {last_name}"
    return full_name.title()

musician = get_formatted_name('jimi', 'hendrix')
print(musician)

# Making an Argument Optional

# Sometimes we want to allow for optional data within a function, in the following example, the middle name argument is optional:

def get_formatted_name(first_name, last_name, middle_name=''):
    """Return a full name, neatly formatted"""
    if middle_name:
        full_name = f"{first_name} {middle_name} {last_name}"
    else:
        full_name = f"{first_name} {last_name}"
    return full_name.title()

# Because the middle_name argument is made optional using the ='' syntax, this will not throw an error:
musician = get_formatted_name('jimi', 'hendrix')
print(musician)

# And of course, this works just fine as well:
musician = get_formatted_name('john', 'hooker', 'lee')
print(musician)

# Returning a Dictionary

def build_person(first_name, last_name):
    """Return a dictionary of information about a person."""
    person = {'first': first_name, 'last': last_name}
    return person

musician = build_person('jimi', 'hendrix')
# Returns
# {'first': 'jimi', 'last': 'hendrix'}
print(musician)

# There is another way of allowing for an optional argument in functions as well using the None keyword:
# This is probably better, since it doesn't even predefine a type, whereas above it expects a string
def build_person(first_name, last_name, age=None):
    """Return a dictionary of information about a person."""
    person = {'first': first_name, 'last': last_name}

    if age:
        person['age'] = age
    return person

musician = build_person('jimi', 'hendrix', age=27)
print(musician)

# Using a Function with a while Loop
# This function looks complicated but upon inspection can be found somewhat simple,
# given what we know so far, it's our get_formatted_name() function that is then put within a while loop
# that defines parameters through user input and then applied to our function afterwards
# def get_formatted_name(first_name, last_name):
    # """Return a full name, neatly formatted."""
    # full_name = f"{first_name} {last_name}"
    # return full_name.title()

# while True:
    # print("\nPlease tell me your name:")
    # print("(enter 'q' at any time to quit)")

    # f_name = input("First name: ")
    # if f_name == 'q':
        # break
    # l_name = input("Last name: ")
    # if l_name == 'q':
        # break

    # formatted_name = get_formatted_name(f_name, l_name)
    # print(f"\nHello, {formatted_name}")

def city_country(city_name, country):
    my_city = f"{city_name} {country}"
    return my_city.title()

final_answer = city_country('santiago', 'chile')
print(final_answer)

# def make_album(artist_name, album_title):
    # final_dict = {'name': artist_name.title(), 'title': album_title.title()}
    # return final_dict

# my_favorite_musician = make_album('zedd', 'true colors')
# print(my_favorite_musician)

# while True:
    # print("\nPlease enter your favorite artist and album title:")
    # print("\n(enter 'q' at any time to quit)")

    # a_name = input("Artist Name: ")
    # if a_name == 'q':
        # break
    # a_title = input("Album Title: ")
    # if a_title == 'q':
        # break

    # final_object = make_album(a_name, a_title)
    # print(final_object)

# Passing a List

# We can, of course, pass a list as an argument to a function, iterate over it and do something with it,
# in this case, we pass a list of names, and on each name, we create a custom message that capitalizes the passed name,
#and prints it to the console

def greet_users(names):
    """Print a simple greeting to each user in the list."""
    for name in names:
        msg = f"Hello, {name.title()}!"
        print(msg)

usernames = ['hannah', 'ty', 'margot']
greet_users(usernames)

# Modifying a List in a Function

# The function below simply does what one of our other functions does above, takes a list and appends it to another while emptying out
# the original list, these use the pop() and append() methods respectively, nothing too remarkable here...

# Start with some designs that need to be printed.
unprinted_designs = ['phone case', 'robot pendant', 'dodecahedron']
completed_models =[]

# Simulate printing each design, until none are left.
# Move each design to completed_models after printing.
while unprinted_designs:
    current_design = unprinted_designs.pop()
    print(f"Printing model: {current_design}")
    completed_models.append(current_design)

# Display all completed models.
print("\nThe following models have been printed:")
for completed_model in completed_models:
    print(completed_model)

# But we can reorganize this code by writing two functions like so:

# As you can clearly see, the first function print_models() takes 
# two lists, unprinted_designs, and completed_models, and simply
# pops off each element in unprinted_designs until there isn't any left..
# and appends it to the completed_models list

def print_models(unprinted_designs, completed_models):
    """
    Simulate printing each design, until none are left.
    Move each design to completed_models after printing.
    """

    while unprinted_designs:
        current_design = unprinted_designs.pop()
        print(f"Printing model: {current_design}")
        completed_models.append(current_design)

# The second function, show_completed_models() takes in the completed_models list, now full of the items,
# and prints it with a message to the console.

def show_completed_models(completed_models):
    """Show all the models that were printed"""
    print("\nThe following models have been printed:")
    for completed_model in completed_models:
        print(completed_model)

unprinted_designs = ['phone case', 'robot pendant', 'dodecahedron']
completed_models = []

print_models(unprinted_designs, completed_models)
show_completed_models(completed_models)

print("\n")

# While more verbose than the first example (with only one function) this could be conceived as being a better
# design, as each function is concerned with only one task, the first with letting the user know how the first list(unprinted_designs)
# is being popped and appended to the completed_models list
# while the second function is concerned solely with letting the user know when the conversion process is completed

# Preventing a Function from Modifying a List

# In the above example we basically popped off the end of one list and appended it to the end of another, but what if we wanted to
# keep that original list in tact?  In other words, what if we wanted to make that list constant?
# As mentioned in a previous tutorial, Python doesn't allow for constant variables, so this is not truly possible, BUT, let's say
# we needed to create that duplicate list while keeping the original in tact?  Of course we coupld simple copy the list, but this is inefficient
# and can lead to errors, instead we could use the SLICE feature of Python, which functions similarly to JavaScreipt's slice() method, but has
# much different syntax

# def function_name(list_name[:])

# so instead above our print_models() function would be changed to look like this:

# print_models(unprinted_designs[:], completed models)
messages = ['hi there', 'this is jon snow!', "i'm in love with my aunt", "i am not actually a bastard, thank goodness", 'i kill everyone i love...']
# def show_messages():
    # for message in messages:
        # print(message)

# show_messages()

sent_messages = []

# I intentionally deviated here to explore what the opposite of the pop() and append() methods were, they are displayed below
def send_messages(messages, sent_messages):
    while messages:
        # opposite of pop() is pop(0)
        sent_message = messages.pop(0)
        print(f"Sending message: {sent_message}")
        # opposite of append() is insert(0, element)
        sent_messages.insert(0, sent_message)

send_messages(messages[:], sent_messages)

print(messages)
print(sent_messages)
print("\n")

# Passing an Arbitrary Number of Arguments, sometimes we don't know how many arguments we want to pass, in which case, we can use
# an asterix to refer to a list of variables, note that this does NOT mean that we PASS a list, but rather that we pass a series of values
# directly as arguments, but can refer to as a list within the function (meaning that we can iterate over the *arguments)

def make_pizza(*toppings):
    """Print the list of toppings that have been requested."""
    print(toppings)

make_pizza('pepperoni')
make_pizza('mushrooms', 'green peppers', 'extra cheese')

def make_pizza(*toppings):
    """Summarize the pizza we are about to make."""
    print("\nMaking a pizza with the following toppings:")
    for topping in toppings:
        print(f"- {topping}")

make_pizza('pepperoni')
make_pizza('mushrooms', 'green peppers', 'extra cheese')

# Mixing Positional and Arbitrary Arguments
# Should you wish to utilize an arbitrary number of arguments, you can do so, but you MUST pass the arbitrary *arguments LAST
# in your function definition's parameters:

def make_pizza(size, *toppings):
    """Summarize the pizza we are about to make."""
    print(f"\nMaking a {size}-inch pizza with the following toppings:")
    for topping in toppings:
        print(f"- {topping}")

make_pizza(16, 'pepperoni')
make_pizza(12, 'mushrooms', 'green peppers', 'extra cheese')

# Using Arbitrary Keyword Arguments
# Should you need to pass not only an arbitrary number of *arguments,
# but you ALSO will need to pass a series of values with an unknown TYPE
# you can pass a series of key-value pairs, as many as the statement provides, followed by a double asterix list **variable


# Essentially thsi will build a dictionary called user_info
def build_profile(first, last, **user_info):
    """Build a dictionary containing everything we know about a user."""
    # user_info['first_name'] is assigned whatever is passed to the first parameter
    user_info['first_name'] = first
    # user_info['last_name'] is assigned whatever is passed to the second parameter as well
    user_info['last_name'] = last
    return user_info

# Here we finally call build_profile and assign the returned value to user_profile, it creates an object where the first name and last name are expected,
# then using the argument= syntax, a location and a field are added as other key/value pairs, we could continue on adding as many key/value pairs if so desired
user_profile = build_profile('albert', 'einstein', location= 'princeton', field= 'physics')
print(user_profile)

print("\n")
# Sandwiches

def make_sandwich(*deli_items):
    """Build a sandwich with an arbitrary number of deli items"""
    print(f"Making a sandwich with the following ingredients:")
    for item in deli_items:
        print(f"- {item}")

make_sandwich('ham', 'cheese')
make_sandwich('salami', 'provolone', 'lettuce')
make_sandwich('peanut-butter', 'jelly', 'honey', 'bacon')

print("\n")

# User Profile

def build_profile(first, last, **user_info):
    """Build a dictionary containing everything we know about a user."""
    # user_info['first_name'] is assigned whatever is passed to the first parameter
    user_info['first_name'] = first
    # user_info['last_name'] is assigned whatever is passed to the second parameter as well
    user_info['last_name'] = last
    return user_info

my_profile = build_profile('brian', 'hayes', sport= 'handlball', language= 'english', nationality= 'american')
print(my_profile)

print("\n")

# Cars

def make_car(manufacturer, model, **car):
    car['manufacturer'] = manufacturer
    car['model_name'] = model
    return car

car = make_car('subaru', 'outback', color= 'blue', tow_package=True)
print(car)

# Storing Your Functions in Modules

# This is where we start getting organized, here we demonstrate the importing/exporting of functions from different files
# From here navigate to the modules directory
